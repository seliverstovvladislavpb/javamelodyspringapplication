package com.java.melody.application.service;

import com.intern.database.dao.UrlDAO;
import com.intern.database.model.Url;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Service for url entities.
 */
@Service
public class UrlService {

    /**
     * Data access object.
     */
    UrlDAO urlDAO;

    @Autowired
    public UrlService(UrlDAO urlDAO) {
        this.urlDAO = urlDAO;
    }

    /**
     * Get all url objects from database.
     *
     * @return List of url objects.
     */
    public List<Url> findAll() {
        return urlDAO.findAll();
    }

    /**
     * Get url by id.
     *
     * @param id id of the url.
     * @return url object with given id.
     */
    public Url findById(Long id) {
        Optional<Url> urlOptional = urlDAO.findById(id);
        if(urlOptional.isPresent())
            return urlOptional.get();
        return null;
    }

    /**
     * Get url by address.
     *
     * @param address address of th url.
     * @return url object with given address.
     */
    public Url findByAddress(String address) {
        return urlDAO.findByAddres(address);
    }

    /**
     * Save new url object to database.
     *
     * @param url url object to save.
     * @return saved instance of url object.
     */
    public Url save(Url url) {
        return urlDAO.save(url);
    }

    /**
     * Update url object in database.
     *
     * @param url new url object data to update.
     * @param id id of the entity in database, which need to be updated.
     * @return updated instance of url object.
     */
    public Url update(Url url, Long id) {
        url.setId(id);
        return urlDAO.save(url);
    }

    /**
     * Delete url object from database.
     *
     * @param id id of the url object in database.
     */
    public void delete(Long id) {
        Url url = findById(id);
        if(url != null)
            urlDAO.deleteById(id);
    }
}
