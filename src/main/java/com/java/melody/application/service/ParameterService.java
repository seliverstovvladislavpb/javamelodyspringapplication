package com.java.melody.application.service;

import com.intern.database.dao.ParameterDAO;
import com.intern.database.model.Parameter;
import org.hibernate.validator.constraints.EAN;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Service for parameter entities.
 */
@Service
public class ParameterService {
    /**
     * Data access object.
     */
    ParameterDAO parameterDAO;

    @Autowired
    public ParameterService(ParameterDAO parameterDAO) {
        this.parameterDAO = parameterDAO;
    }

    /**
     * Get parameter by id.
     * @param id id of the parameter.
     *
     * @return parameter with given id.
     */
    public Parameter findById(Long id) {
        Optional<Parameter> parameterOptional = parameterDAO.findById(id);
        if(parameterOptional.isPresent())
            return parameterOptional.get();
        return null;
    }

    /**
     * Get parameter by name.
     *
     * @param name name of the parameter.
     * @return parameter with given name.
     */
    public Parameter findByName(String name) {
        return parameterDAO.findByName(name);
    }

    /**
     * Get all parameters.
     *
     * @return List of parameters.
     */
    public List<Parameter> findAll() {
        return parameterDAO.findAll();
    }

    /**
     * Delete parameter from database.
     *
     * @param id id of the parameter.
     */
    public void delete(Long id) {
        Parameter parameter = findById(id);
        if(parameter != null)
            parameterDAO.deleteById(id);
    }

    /**
     * Save new parameter to database.
     *
     * @param parameter parameter entity.
     * @return Saved instance of parameter.
     */
    public Parameter save(Parameter parameter) {
        return parameterDAO.save(parameter);
    }

    /**
     * Update parameter data in database.
     *
     * @param parameter entity with updated data.
     * @return updated instance of the parameter.
     */
    public Parameter update(Parameter parameter) {
        return parameterDAO.save(parameter);
    }
}
