package com.java.melody.application.service;

import com.intern.database.dao.ConnectionDAO;
import com.intern.database.dao.UrlDAO;
import com.intern.database.model.ConnectionToDatasource;
import com.intern.database.model.Url;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Service for connection entities.
 */
@Service
public class ConnectionService {

    /**
     * Data access object.
     */
    ConnectionDAO connectionDAO;
    UrlDAO urlDAO;

    @Autowired
    public ConnectionService(ConnectionDAO connectionDAO) {
        this.connectionDAO = connectionDAO;
    }

    /**
     * Get all connection to datasource.
     * @return List of all connections to datasource.
     */
    public List<ConnectionToDatasource> findAll() {
        return connectionDAO.findAll();
    }

    /**
     * Get connection to datasource by id.
     *
     * @param id id of the connection to datasource.
     * @return connection to datasource with given id.
     */
    public ConnectionToDatasource findById(Long id) {
        Optional<ConnectionToDatasource> connectionToDatasourceOptional = connectionDAO.findById(id);
        if(connectionToDatasourceOptional.isPresent())
            return connectionToDatasourceOptional.get();
        return null;
    }

    /**
     * Get connection to datasource by url and time of the connection.
     *
     * @param url url of datasource.
     * @param time tine of the connection to datasource.
     * @return connection to datasource with given url and time.
     */
    public ConnectionToDatasource findByUrlAndTime(Url url, LocalDateTime time) {
        return connectionDAO.findByUrlAndTime(url, time);
    }

    /**
     * Save new connection to datasource entity to database.
     *
     * @param connection connection to datasource entity.
     * @return saved instance of connection to datasource.
     */
    public ConnectionToDatasource save(ConnectionToDatasource connection) {
        Url connectionUrl = urlDAO.findByAddres(connection.getUrl().getUrlAddres());
        if(connectionUrl==null) {
            connectionUrl = urlDAO.save(connection.getUrl());
            connection.setUrl(connectionUrl);
        }
        return connectionDAO.save(connection);
    }

    /**
     * Delete connection to datasource data from database.
     *
     * @param id id of the connection to datasource.
     */
    public void delete(Long id) {
        ConnectionToDatasource connectionToDatasource = findById(id);
        if(connectionToDatasource != null)
            connectionDAO.deleteById(id);
    }

    /**
     * Update connection to datasource data in database.
     *
     * @param connection new connection data.
     * @param id id of the connection in database which need to update.
     * @return updated instance of connection to datasource.
     */
    public ConnectionToDatasource update(ConnectionToDatasource connection, Long id) {
        connection.setConnectionId(id);
        return connectionDAO.save(connection);
    }
}
