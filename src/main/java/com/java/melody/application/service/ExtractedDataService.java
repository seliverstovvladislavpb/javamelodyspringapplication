package com.java.melody.application.service;

import com.intern.database.dao.ExtractedDataDAO;
import com.intern.database.model.ExtractedData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Service
public class ExtractedDataService {

    private ExtractedDataDAO extractedDataDAO;

    @Autowired
    public ExtractedDataService(ExtractedDataDAO extractedDataDAO) {
        this.extractedDataDAO = extractedDataDAO;
    }

    public List<ExtractedData> findAll() {
        return extractedDataDAO.findAll();
    }
}
