package com.java.melody.application.resource;

import com.intern.database.model.Parameter;
import com.java.melody.application.domain.DTO.ParameterCreationDTO;
import com.java.melody.application.domain.DTO.ParameterUpdateDTO;
import com.java.melody.application.service.ParameterService;
import com.java.melody.application.util.DTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Resource endpoint for parameter entities.
 */
@RestController
@RequestMapping("api/parameters")
public class ParameterResource {

    ParameterService parameterService;

    @Autowired
    public ParameterResource(ParameterService parameterService) {
        this.parameterService = parameterService;
    }

    /**
     * Get all parameters.
     *
     * @return JSON list of parameters.
     */
    @GetMapping("/all")
    public List<Parameter> findAll() {
        return parameterService.findAll();
    }

    /**
     * Get parameter with given id.
     *
     * @param id id of the parameter.
     * @return JSON object of parameter.
     */
    @GetMapping("/find/{id}")
    public Parameter findById(@PathVariable("id") Long id) {
        return parameterService.findById(id);
    }

    /**
     * Find parameter by name.
     *
     * @param name name of the parameter.
     * @return parameter with given name.
     */
    @GetMapping(value = "/find/", produces="application/json", consumes="application/json")
    public Parameter findByName(@RequestParam String name) {
        return parameterService.findByName(name);
    }

    /**
     * Save new parameter.
     *
     * @param parameter parameter entity.
     * @return saved parameter.
     */
    @PostMapping("/save")
    public Parameter save(@DTO(ParameterCreationDTO.class) Parameter parameter) {
        return parameterService.save(parameter);
    }

    /**
     * Update parameter.
     *
     * @param parameter parameter entity.
     * @return updated parameter.
     */
    @PutMapping("/update")
    public Parameter update(@DTO(ParameterUpdateDTO.class) Parameter parameter) {
        return parameterService.update(parameter);
    }

    /**
     * Delete parameter by id.
     *
     * @param id id of the parameter.
     */
    @GetMapping("/delete/{id}")
    public void delete(@PathVariable("id") Long id)
    {
        parameterService.delete(id);
    }
}
