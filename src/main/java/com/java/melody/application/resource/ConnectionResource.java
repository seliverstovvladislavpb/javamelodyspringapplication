package com.java.melody.application.resource;

import com.intern.database.model.ConnectionToDatasource;
import com.java.melody.application.service.ConnectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller for connection resource.
 */
@RestController
@RequestMapping("/api/connection")
public class ConnectionResource {

    ConnectionService connectionService;
    @Autowired
    public ConnectionResource(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    /**
     * Get all connections.
     * @return list of all connections.
     */
    @GetMapping("/all")
    public List<ConnectionToDatasource> findAll() {
        return connectionService.findAll();
    }

    /**
     * Find connection by id.
     * @param id id of the connection.
     * @return connection with given id.
     */
    @GetMapping("/{id}")
    public ConnectionToDatasource findById(@PathVariable(name = "id")Long id) {
        return connectionService.findById(id);
    }

    /**
     * Delete connection by id.
     * @param id id of the connection to delete.
     */
    @GetMapping("/delete/{id}")
    public void deleteById(@PathVariable(name = "id") Long id)
    {
        connectionService.delete(id);
    }

    /**
     * Add new connection to database.
     * @param connectionToDatasource new connection data object.
     * @return saved instance of connection to datasource object.
     */
    @PostMapping("/add")
    public ConnectionToDatasource save(@RequestBody ConnectionToDatasource connectionToDatasource) {
        return connectionService.save(connectionToDatasource);
    }

    /**
     * Update connection data in database.
     * @param connectionToDatasource new connection data.
     * @param id connection id to update.
     * @return updated instance of connection object.
     */
    @PutMapping("/update/{id}")
    public ConnectionToDatasource update(@RequestBody ConnectionToDatasource connectionToDatasource,
                                         @PathVariable(name = "id") Long id) {
        connectionToDatasource.setConnectionId(id);
        return connectionService.save(connectionToDatasource);
    }
}
