package com.java.melody.application.resource;

import com.intern.database.model.Url;
import com.java.melody.application.service.UrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/url")
public class UrlResource {

    UrlService urlService;

    @Autowired
    public UrlResource(UrlService urlService) {
        this.urlService = urlService;
    }

    @GetMapping("/all")
    public List<Url> findAll() {
        return urlService.findAll();
    }

    @GetMapping("/find/{id}")
    public Url findById(@PathVariable("id")Long id) {
        return urlService.findById(id);
    }

    @GetMapping("/find/by-address")
    public Url findByAddress(@RequestParam String address) {
        return urlService.findByAddress(address);
    }

    @PostMapping("/save")
    public Url save(@RequestBody Url url) {
        return urlService.save(url);
    }

    @GetMapping("/delete/{id}")
    public void delete(@PathVariable("id") Long id){
        urlService.delete(id);
    }

    @PostMapping("/update/{id}")
    public Url update(Url url, @PathVariable("id")Long id) {
        return urlService.update(url, id);
    }
}
