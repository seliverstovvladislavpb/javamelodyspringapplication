package com.java.melody.application.domain.DTO;

import lombok.*;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ParameterUpdateDTO {

    @Id
    @NotNull
    private Long parameterId;

    @NotNull
    private String parameterName;

    @NotNull
    private String parameterValueType;

    @NotNull
    private String parameterValueDimmension;
}
