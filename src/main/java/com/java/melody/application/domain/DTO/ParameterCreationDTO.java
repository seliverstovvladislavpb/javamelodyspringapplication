package com.java.melody.application.domain.DTO;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ParameterCreationDTO {

    @NotNull
    private String parameterName;

    @NotNull
    private String parameterValueType;

    @NotNull
    private String parameterValueDimmension;
}
